----------------public-------------------
bool containsDuplicate(vector<int>& nums) {
    for (int i = 0; i < nums.size(); i++) {
      for (int j = i + 1; j < nums.size(); j++) {
        if (nums[i] == nums[j]) {
          return true; // if any two elements are the same, return true
        }
      }
    }
    return false; // if no duplicates are found, return false
  }
  ------------------------------------------------------
